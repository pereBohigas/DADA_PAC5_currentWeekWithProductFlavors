package edu.uoc.android.currentweek.utilities;

import java.util.Calendar;

public class DateUtil {

    private final Calendar calendar;

    public DateUtil (Calendar calendar) {
        this.calendar = calendar;
    }

    public boolean isTheCurrentWeekNumber (int weekNumber) {
        return false;
    }

    private int getCurrentWeekNumber() {
        return calendar.get(Calendar.WEEK_OF_YEAR);
    }

}
